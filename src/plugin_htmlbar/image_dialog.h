/* OwlGalunggung Code Editor
 * image_dialog.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 

#ifndef __IMAGE_DIALOG_H_
#define __IMAGE_DIALOG_H_

#include "../owlgalunggung.h"

#include "html_diag.h"

#define OWLGALUNGGUNG_TYPE_IMAGE_DIALOG            (owlgalunggung_image_dialog_get_type ())
#define OWLGALUNGGUNG_IMAGE_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), OWLGALUNGGUNG_TYPE_IMAGE_DIALOG, OwlGalunggungImageDialog))
#define OWLGALUNGGUNG_IMAGE_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), OWLGALUNGGUNG_TYPE_IMAGE_DIALOG, OwlGalunggungImageDialogClass))
#define OWLGALUNGGUNG_IS_IMAGE_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), OWLGALUNGGUNG_TYPE_IMAGE_DIALOG))
#define OWLGALUNGGUNG_IS_IMAGE_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), OWLGALUNGGUNG_TYPE_IMAGE_DIALOG))
#define OWLGALUNGGUNG_IMAGE_DIALOG_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), OWLGALUNGGUNG_TYPE_IMAGE_DIALOG, OwlGalunggungImageDialogClass))


typedef struct _OwlGalunggungImageDialogPrivate OwlGalunggungImageDialogPrivate;

typedef struct _OwlGalunggungImageDialog OwlGalunggungImageDialog;
  
struct _OwlGalunggungImageDialog
{
  GtkDialog parent;
  
  OwlGalunggungImageDialogPrivate *priv;
};

typedef struct _OwlGalunggungImageDialogClass OwlGalunggungImageDialogClass;

struct _OwlGalunggungImageDialogClass
{
  GtkDialogClass parent_class;
};

GType owlgalunggung_image_dialog_get_type (void);

void owlgalunggung_image_dialog_new (Towlgalwin *owlgalwin);

void owlgalunggung_image_dialog_new_with_data (Towlgalwin *owlgalwin,
																					Ttagpopup *data);

#endif /* __IMAGE_DIALOG_H_ */
