/*
 * OwlGalunggung Code Editor
 * owlgalwin_uimanager.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OWLGALWIN_UIMANAGER_H_
#define OWLGALWIN_UIMANAGER_H_

#include "owlgalunggung.h"
#include "owlgalwin.h"


#define OWLGAL_RECENT_FILE_GROUP		"owlgalunggung-recent-file"
#define OWLGAL_RECENT_PROJECT_GROUP		"owlgalunggung-recent-project"

void sync_fullscreen_toggle(Towlgalwin *owlgalwin, gboolean is_fullscreen);

void owlgalwin_main_ui_init(Towlgalwin * owlgalwin, GtkWidget * vbox);
void owlgalwin_set_undo_redo_actions(Towlgalwin * owlgalwin, gboolean undo, gboolean redo);
void owlgalwin_set_cutcopypaste_actions(Towlgalwin * owlgalwin, gboolean enabled);
void owlgalwin_set_document_menu_items(Tdocument * doc);

void owlgalwin_action_set_sensitive(GtkUIManager * manager, const gchar * path, gboolean sensitive);

void owlgalwin_set_menu_toggle_item(GtkActionGroup * action_group, const gchar * action_name, gboolean is_active);
void owlgalwin_set_menu_toggle_item_from_path(GtkUIManager * manager, const gchar * path, gboolean is_active);

void owlgalwin_encoding_set_wo_activate(Towlgalwin * owlgalwin, const gchar * encoding);
void owlgalwin_lang_mode_set_wo_activate(Towlgalwin * owlgalwin, Towlgallang * owlgallang);

void owlgalwin_commands_menu_create(Towlgalwin * owlgalwin);
void owlgalwin_encodings_menu_create(Towlgalwin * owlgalwin);
void owlgalwin_filters_menu_create(Towlgalwin * owlgalwin);
void owlgalwin_outputbox_menu_create(Towlgalwin * owlgalwin);
void lang_mode_menu_create(Towlgalwin * owlgalwin);

/*void owlgalwin_recent_menu_add(Towlgalwin * owlgalwin, GFile * file, GFileInfo * finfo, gboolean is_project);*/
void owlgalwin_recent_menu_add(Towlgalwin *owlgalwin, gboolean project, const gchar *curi, GFile *uri);
void owlgalwin_recent_menu_remove(Towlgalwin *owlgalwin, gboolean project, const gchar *curi, GFile *uri);
void owlgalwin_recent_menu_create(Towlgalwin *owlgalwin, gboolean only_update_session);
void owlgalwin_templates_menu_create(Towlgalwin * owlgalwin);

void sync_fullscreen_toggle(Towlgalwin *owlgalwin, gboolean is_fullscreen);
#ifdef MAC_INTEGRATION
void owlgalwin_action_groups_set_sensitive(Towlgalwin * owlgalwin, gboolean sensitive);
gboolean owlgalwin_action_group_is_available(GtkUIManager * manager, gchar * action_group_name);
#endif

#endif							/* OWLGALWIN_UIMANAGER_H_ */
