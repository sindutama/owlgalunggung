/* OwlGalunggung Code Editor
 * project.h - project prototypes
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROJECT_H_
#define __PROJECT_H_

void project_edit(Towlgalwin * owlgalwin);
void project_new(Towlgalwin * owlgalwin);
void project_open(Towlgalwin * owlgalwin);
gint project_open_from_file(Towlgalwin * owlgalwin, GFile * fromuri);

gboolean project_save(Towlgalwin * owlgalwin, gboolean save_as);
void project_save_and_close(Towlgalwin * owlgalwin);
void project_save_and_mark_closed(Towlgalwin * owlgalwin);
gboolean project_final_close(Towlgalwin * owlgalwin, gboolean close_win);

void set_project_menu_actions(Towlgalwin * owlgalwin, gboolean win_has_project);

#endif							/* __PROJECT_H_  */
