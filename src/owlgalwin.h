/*
 * OwlGalunggung Code Editor
 * owlgalwin.h
 *
 * Copyright (C) 2017-2018 Hyang Language Foundation
 * Copyright (C) 2011 James Hayward
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OWLGALWIN_H_
#define OWLGALWIN_H_

#include "owlgalunggung.h"

typedef void (*DocDestroyCallback) (Tdocument *doc, gpointer data);
typedef void (*CurdocChangedCallback) (Towlgalwin *owlgalwin, Tdocument *olddoc, Tdocument *newdoc, gpointer data);
typedef void (*DocDeleteRangeCallback) (Tdocument *doc, GtkTextIter * itstart, gint start, GtkTextIter * itend, gint end, const gchar *string, gpointer data);
typedef void (*DocInsertTextCallback) (Tdocument *doc, const gchar *string, GtkTextIter * iter, gint pos, gint len, gint clen, gpointer data);

#define owlgalwin_exists(owlgalwin) (g_list_index(main_v->owlgalwinlist, owlgalwin)!=-1)

void owlgalwin_fullscreen_toggle(Towlgalwin * owlgalwin, gboolean active);
void owlgalwin_gotoline_search_bar_close(Towlgalwin *owlgalwin, gboolean clean_entry_only);
void owlgalwin_gotoline_from_clipboard(Towlgalwin * owlgalwin);
void owlgalwin_gotoline_frame_show(Towlgalwin * owlgalwin);
void owlgalwin_simplesearch_show(Towlgalwin *owlgalwin);
void owlgalwin_simplesearch_from_clipboard(Towlgalwin *owlgalwin);

void notebook_set_tab_accels(Towlgalwin * owlgalwin);
void notebook_bind_tab_signals(Towlgalwin * owlgalwin);
void notebook_unbind_tab_signals(Towlgalwin * owlgalwin);
void owlgalwin_notebook_block_signals(Towlgalwin * owlgalwin);
void owlgalwin_notebook_unblock_signals(Towlgalwin * owlgalwin);

void owlgalwin_current_document_change_register(Towlgalwin *owlgalwin, CurdocChangedCallback func, gpointer data);
void owlgalwin_current_document_change_remove_by_data(Towlgalwin *owlgalwin, gpointer data);
void owlgalwin_current_document_change_remove_all(Towlgalwin *owlgalwin);
void owlgalwin_document_insert_text_register(Towlgalwin *owlgalwin, DocInsertTextCallback func, gpointer data);
void owlgalwin_document_insert_text_remove_by_data(Towlgalwin *owlgalwin, gpointer data);
void owlgalwin_document_delete_range_register(Towlgalwin *owlgalwin, DocDeleteRangeCallback func, gpointer data);
void owlgalwin_document_delete_range_remove_by_data(Towlgalwin *owlgalwin, gpointer data);
void owlgalwin_document_destroy_register(Towlgalwin *owlgalwin, DocDestroyCallback func, gpointer data);
void owlgalwin_document_destroy_remove_by_data(Towlgalwin *owlgalwin, gpointer data);

void owlgalwin_notebook_changed(Towlgalwin * owlgalwin, gint newpage);
void owlgalwin_notebook_switch(Towlgalwin * owlgalwin, guint action);

void owlgalwin_main_toolbar_show(Towlgalwin * owlgalwin, gboolean active);

void owlgalwin_output_pane_show(Towlgalwin * owlgalwin, gboolean active);

gboolean owlgalwin_side_panel_show_hide_toggle(Towlgalwin * owlgalwin, gboolean first_time, gboolean show,
										   gboolean sync_menu);
void owlgalwin_side_panel_show(Towlgalwin * owlgalwin, gboolean active);
void owlgalwin_side_panel_rebuild(Towlgalwin * owlgalwin);

void owlgalwin_statusbar_show(Towlgalwin * owlgalwin, gboolean active);

void owlgalwin_apply_session(Towlgalwin * owlgalwin, Tdocument *active_doc);
void owlgalwin_apply_settings(Towlgalwin * owlgalwin);
void owlgalwin_destroy_and_cleanup(Towlgalwin *owlgalwin);
gboolean owlgalwin_delete_event(GtkWidget * widget, GdkEvent * event, Towlgalwin * owlgalwin);
#ifdef MAC_INTEGRATION
gboolean owlgalwin_osx_terminate_event(GtkWidget * widget, GdkEvent * event, Towlgalwin * owlgalwin);
#endif

void owlgalwin_create_main(Towlgalwin * owlgalwin);
void owlgalwin_show_main(Towlgalwin * owlgalwin);

void owlgalwin_statusbar_message(Towlgalwin * owlgalwin, const gchar * message, gint seconds);
void owlgalwin_all_statusbar_message(const gchar * message, gint seconds);

void owlgalwin_window_close(Towlgalwin * owlgalwin);
Towlgalwin *owlgalwin_window_new(void);
Towlgalwin *owlgalwin_window_new_with_project(Tproject * project);

gboolean owlgalwin_has_doc(Towlgalwin * owlgalwin, Tdocument * doc);
void owlgalwin_docs_not_complete(Towlgalwin * owlgalwin, gboolean increase);
void owlgalwin_set_title(Towlgalwin * owlgalwin, Tdocument * doc, gint num_modified_change);

gboolean owlgalwin_switch_to_document_by_index(Towlgalwin * owlgalwin, gint index);
gboolean owlgalwin_switch_to_document_by_pointer(Towlgalwin * owlgalwin, Tdocument * document);

#endif							/* OWLGALWIN_H_ */
